# Ring Light for Camera

## Layman Description

A DIY Ring Light for various cameras, DLSR or MLIC to add more light source in macro photography.

## Product Description

## Why I Built it

A photographer can use built-in flash or external flash on the hot sock interface during filming. 
Micro photography is special. The light from the flash is too strong. In same cases, The len itself obscures the light that the flash hits on the subject.

I have to do the photography by myself, for I've been selling products in this store. Additionally,
even the popular mobile phone photography still need extra light source as well. Therefore, I designed
the low cost ring light for cameras.

The ring light is made up two parts: light source and control board with optional battery pack.

## Features

- Full color LED matrix
- RISC-V MCU to control color temperature, brightness, and even colors
- Quick changing via user buttons
- User defined parameters via USB
- User can change the firmware to add more features such as animation of LED.

## Known Restrictions

## Reference Web Resources

- [Make music EQ display with CH32V003 and WS212 (Chinese)](https://www.cnblogs.com/wahahahehehe/p/16853802.html)
- [A CH32V003 based WS2812 LED in oshwhub (Chinese)](https://oshwhub.com/navigatorkepler/ch32v003f4u6-pwm-fan-switch)
- [Pixel Pushing using DMA on the CH32V003 (No source)](https://hackaday.io/project/194373-pixel-pushing-using-dma-on-the-ch32v003)
- [RISC-V IDE MounRiver Studio: TWEN32V RGB](https://blog.csdn.net/qq_36353650/article/details/123271182)
