# Ring Light for Camera

## Layman Description

A DIY Ring Light for various cameras, DLSR or MLIC to add more light source in macro photography.

## Product Description

## Why I Built it

A photographer can use built-in flash or external flash on the hot sock interface during filming. 
Micro photography is special. The light from the flash is too strong. In same cases, The len itself obscures the light that the flash hits on the subject.

I have to do the photography by myself, for I've been selling products in this store. Additionally,
even the popular mobile phone photography still need extra light source as well. Therefore, I designed
the low cost ring light for cameras.

The ring light is made up two parts: light source and control board with optional battery pack.

## Features

- PWM driven white LED strips
- RISC-V MCU to control brightness
- Quick changing via user buttons
- User defined parameters via USB
- User can change the firmware to add more features such as animation of LED.

## Known Restrictions

## Reference Web Resources

- [WCH RISC-V MCU](https://wch.cn/)