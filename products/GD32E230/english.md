## 32bit ARM Cortex-M23 core

Up to 72 MHz operation frequency
Single-cycle multiplication and hardware diviEder
Ultra-low power, energy-efficient operation
Excellent code density
Integrated Nested Vectored Interrupt Controller (NVIC)
24-bit SysTick timer
Internal Bus Matrix connected with AHB master, Serial Wire Debug Port and Single-cycle
IO port
Nested Vectored Interrupt Controller (NVIC)
Breakpoint Unit(BPU)
Data Watchpoint and Trace (DWT)
Serial Wire Debug Port

## Embedded Flash
Up to 64 Kbytes of Flash memory
Up to 8 Kbytes of SRAM with hardware parity checking

## Clock
Internal 8 MHz factory-trimmed RC and external 4 to 32 MHz crystal oscillator
Internal 28 MHz RC oscillator
Internal 40 KHz RC calibrated oscillator and external 32.768 KHz crystal oscillator
Integrated system clock PLL
1.8 to 3.6 V application supply and I/Os

## Boot
Boot from main flash for user code
Boot from system memory for boot code
Boot from on-chip SRAM

## Power Saving Modes
Sleep, Deep-sleep and Standby modes

## Peripherals
- 12bit SAR ADC on 2Mbps
- 12bit, 10bit, 8bit and 6bit configuration resolution
- Hardware oversampling ratio adjustable from 2x to 256x improves resolution to 16bit.
- Temperature sensor
- 5x DMA between timers, ADC, SPI, I2C, USART and I2S.
- up to 39x GPIO
- 1x 16bit advanced timer, up to 5x 16bit GP timers, 1x basic timer.
- Up to 4x PWM
- 1x motor control PWM 
- Encoder with 2x QDEC
- 24-bit systick timer
- 2x watchdog timers
- RTC with calendar
- 2x I2C up to 1MHz
- 2x SPI up to 18MHz,  support QSPI in master mode
- 2x USART up to 4.5Mbps, support IrDA/LIN/ISO7816-m modes
- 1x I2S from 8kHz to 192kHz
- 1x comparator

## Power Supply
Supply Supervisor: POR (Power On Reset), PDR (Power Down Reset), and low voltage
detector (LVD)

VDD range: 1.8 to 3.6 V, external power supply for I/Os and the internal regulator.
Provided externally through VDD pins.
VSSA, VDDA range: 1.8 to 3.6 V, external analog power supplies for ADC, reset blocks,
RCs and PLL. VDDA and VSSA must be connected to VDD and VSS, respectively.
VBAK range: 1.8 to 3.6 V, power supply for RTC, external clock 32 KHz oscillator and
backup registers (through power switch) when VDD is not present.

## Package
LQFP48 (GD32E230CxTx), LQFP32 (GD32E230KxTx), QFN32 (GD32E230KxUx),
QFN28 (GD32E230GxUx), TSSOP20 (GD32E230FxPx) and LGA20 (GD32E230FxVx).
 Operation temperature range: -40°C to +85°C (industrial level) for grade 6 devices, and
-40°C to +105°C (industrial level) for grade 7 devices.

## Datasheet

- https://www.gigadevice.com.cn/Public/Uploads/uploadfile/files/20230314/GD32E230xxDatasheet_Rev2.0.pdf