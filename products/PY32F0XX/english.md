PY32F030 and PY32F003 are manufactured by PUYA Shanghai, who was known as a low cost SPI flash supplier.

High performance 32-bit ARM Cortex-M0+core and MCU with wide voltage operating range are adopted. The maximum embedded memory can reach 64K bytes flash and 8K bytes SRAM, and the maximum operating frequency can reach 48MHz. It contains a variety of different packaging types and products. The chip integrates multiple I2C, SPI, USART and other communication peripherals, one 12bit ADC, six 16bit timers, and two comparators.

The operating temperature range is -40 ℃~85 ℃, and the operating voltage range is 1.7V~5.5V. The chip provides sleep and stop low-power operation modes, which can meet different low-power applications.

Key features:
- 32bit ARM Cortex-M0+ core
- Up to 64KB (embedded) flash memory
- Up to 4KB SRAM
- Clock subsystem for HSI/HSE/LSI/LSE/PLL
- Clock up to 48MHz
- Operating voltage 1.7~5.5V
- Operating temperature -40~85
- Up to 30 GPIOs
- 1x I2C
- 2x SPI
- 2x USART
- 4x bit GP Timer
- 1x ADC timer
- 1x LP Timer
- 1x 12bit 12 channel ADC
- 2x analog comparator
- LQFP32/QFN32/QFN20/TSSOP20

## Caution

- Someone from the OSHW community complains about its ADC performanc and SPI waveform. Please double check.