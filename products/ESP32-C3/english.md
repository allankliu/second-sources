ESP32-C3 is a multi-mode SoC with WiFi and BLE radios and RISC-V core. This device is manufactured by Expressif in Shanghai.

ESP32 is a famous product line for low-cost WiFi/BLE dual mode IoT SoC. The early models are designed with Xtensa LX soft-core. Now Expressif released a RISC-V based version. Its key features:

- Single core RISC-V SoC running up to 160MHz with 4 level pipelines
- Built-in 400KB SRAM (16KB cache)
- 384KB ROM 
- SPI/Dual SPI/Quad SPI/QPI interfaces to external memories
- Broad temperature range
- RSA3072 based security boot, and AES128/XTS256 code protection
- Digital signature and HMAC modules
- TRNG True random number generator
- IEEE802.11 b/g/n
- 2.4GHz, 20MHz/40MHz bandwidth
- 1T1R, up to 150Mbps
- BLE 5 / BLE mesh on 125K/500K/1M/2Mbps
- 21dBm TX
- 22 GPIO
- 3x SPI
- 2x UART
- 1x I2C
- 1x I2S
- IR transmitter, 2 TX/2RX
- 6 channel LED PWM
- Full speed USB serial/JTAG
- GDMA
- 1x TWAI for CAN2.06 channel
- 1x temperature sensor
- 2x 54bit GP timers
- 3x watchdog timers
- 1x analog watchdog timer
- 1x 52bit systemtick timer
- 2x 12bit SARADC, up to 
- ESP-IDF supported
- ESP-AT/ESP-hosted SDK supported

Restrictions
- Only single core RISC-V, may have performance issue.
- Some code has to be run from SRAM for higher performance.