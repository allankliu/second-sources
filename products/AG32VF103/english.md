The AG32 family of 32-bit microcontrollers is designed to offer new degrees of freedom and rich compatible peripherals, and compatible pin and features to MCU users. 
AG32F103 product series offers supreme quality, stability, and exceptional pricing value.

- AG32VF103 running up to 168MHz
- AG32VF107 running up to 168MHz
- AG32VF205 running up to 184MHz
- AG32VF303 running up to 208MHz
- AG32VF407 running up to 248MHz
- 128KB SRAM
- 16K/32K/64K/128K/256KB, up to 1024KB flash memory
- 5x GP advanced timers
- 2x basic timers
- 1x systick timer
- 1x WDG timer
- 1x RTC timer
- 5x UART
- 2x I2C bus
- 2x SPI bus
- 1x CAN 2.0
- USB2.0 FS/OTG
- 1x Ethernet MAC
- 1x Crypto/Hash
- 3x 12b ADC
- 2x 12b DAC
- 2x comparators
- 2K LEs FPGA
- RAM M9K
- PLL

Base on the selection chart, AG32VFXXX seems like a configurable PSoC with RISC-V core, pin to pin compatible to 
STM32F103/407. It has identical on-chip periperals, with differenet flash sizes and packages. 

The chip is available as AGRV2K100 CPLD. They are the identical chip.
