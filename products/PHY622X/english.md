About PHYPLUS

PHY622X BLE SoC is manufactured by PHYPLUS Shanghai, a leading advanced BLE SoC supplier in China.

Key Features for PHY6222

- Cortex-M0 with SWD (only for debug)
- 128KB-8MB flash memory
- 64KB SRAM, retention in sleep mode
- 4-way instruction cache with 8KB cache RAM
- 96KB ROM (BLE/BLE Mesh stack)
- 256bit efuse
- 22 GPIOs
- GPIO retention in off/sleep mode
- IO MUX function mapping
- All pins can be configured for wake-up
- All pins for triggering interrupt
- 3x QDEC
- 6x PWM
- 2x PDM/I2C/SPI/UART
- 4x DMA
- 8x 12bit ADC with low noise voice PGA
- 6x 24bit timer, one watchdog timer
- RTC (w/o calendar)
- Operating range: 1.8 to 3.6V
- Embdeed buck DC/DC and LDOs
- battery monitor
- RC oscillator hardware calibrations
- Support BLE 2Mbps protocol
- Support Data Length Extension
- Throughput up to 1.6Mbps
- BLE 5.1
- Support SIG-Mesh Multi-Feature
- 2.4GHz transceiver
- -103dBm @ BLE 125Kbps
- TX power -20 to _10dBm in 3dB steps
- AES-128 encryption
- Link layer hardware
- Operation temperatrue: -40 to 125 degree
- RoHS package: QFN32

Restrictions (SPI Flash in XIP mode)

Because embedded flash IP is much expensive than SPI flash memory, more and more SoC starts to use SPI flash memory for both code and data. The code running on SPI flash is called XIP (execution in place) mode. ESP8266/ESP32/RP2040 are using XIP flash as well

As a compromise of XIP flash, the chip has to sacrifise the power consumption, debug, security and clock tree, code execution speed. I have to say it has many restrictions, but it is a real price leader in the market.

Restriction of Toolchain

The code has to be burnt into program flash first before you start to debug. The program software is developed by Phyplus in house, the default language is Chinese. 

Restriction of Memory

Because loading code from XIP flash to RAM is much slower than embedded flash, so PHY622X integrates code cache, but loading code to cache may involve another issue: latency. So some time-critical code has to be run in the RAM, which may reduce useable RAM space for application variables. So it will be an issue if the developer can not address it before making technical decisions.

Restriction of Debug

So far only Keil uVision is supported. Only some special part such as PHY6220 with C-Cky V2 core can be supported by GCC, along with Alibaba.

Restriction of Security

Security has been addressed, secure boot and code protection has been supported.

OpenOCD Programming

Some companies has supported PHY622X devices programming with OpenOCD.