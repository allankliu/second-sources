XY1100 is a hybrid NB-IoT SoC with CMOS PA and software radio technologies, developed by Xinyi (New Wing) Shanghai. The key features are:

- Dual core architecture: MCU + DSP
- A Cortex-M3 running on 240MHz
- 900KB RAM and 417KB user flash
- RX: -134dBm, Tx: 23dBm
- 690-960MHz, 1.71-2.2GHz
- QFN52
- Support OpenCPU software architecture
- Support AT modem interface

Assessment

- It is a low cost NB-IoT SoC with software radio and CMOS PA.
- It can support multiple modem technologies only if you get support from the R&D team.
- It support on board design, instead of module based design.
- It has been verified by major operators in China, so its quality is accepted.

Recommended for large volume applications.

Although it has complete toolchain, but it is key account oriented product, not very friendly to OSHW community.