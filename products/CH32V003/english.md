The CH32V003 is a low cost microcontroller based upon RISC-V2A core. The key features are:

- Qing Ke (highland barley) RISC-V2A core, RV32EC instruction set, up to 48MHz
- 2KB SRAM with 1920B Boot/16KB CodeFlash
- 64B NVMEM area
- 64B user defined information storage
- Power supply range: 3.3~5V
- Various power saving modes: sleep/stop/standy
- Various clock sources: internal trimmed 24MHz, 128KHz RC, external 4~25MHz Xtal
- 7 channle DMA controller
- 1x OpAmp/Comp with ADC/TIM2
- Various timers, 16bit Advanced, 16bit GP, 2 WDGT and 1 systicker
- 1x 10bit ADC in 10 channels
- 1x I2C interface (PMBus/SMBus)
- 1x USART
- 1x SPI interfaces
- 18x GPIO in 3 groups, mapped to 1 external interrupt
- 64bit UID
- Single wire SWDIO debug interface
- Available in TSSOP20/QFN20/SOP16/SOP8/

## Highlight

- OpAmp/Comp on board, which is not common in a low cost micro.
- A complete SDK called EVT is available from the official web.

## Caution

- RISC-V 32EC has not hardware multiplier. The programmer has to double check the mathematics used in the algorithm.
- WCH specific interrupt intruction, has to use the custom GCC.
- The brand-new micro has to be programmed with WCH-LINK, since its boot area is empty.
- It is better not to resue SWIO as IO, unless you understand what you are doing and take risk from doing this.
- There are various extreme low cost boards of CH32V003, but its bootloder is customized and SWIO is locked. For example TWEN online programming platform. Make sure you get a correct version.


