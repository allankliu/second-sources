# RISC-V CH32V003 Revised EVB Kit (<50 chars)

## Layman Description (<140 chars)

A complete enhanced kit for low cost RISC-V CH32V003 with optional debugger and documents 

## Product Description (>250 chars)

## Known Restrictions, First Thing First

- CH32V003 instruction set is optimized for control applications, instead of computation tasks.
- CH32V003 blank chips are shipped without bootloader, use WCH serial debug interface (SDI) debugger to download.
- CH32V003 blank chips can only be programmed by WCH-LINK-E with SDI.
- All documents in English are proofread for global developers.
- More restrictions will be collected and appended here. 

## A Low Cost RISC-V MCU

WCH is well-known for its USB interface IC family. Its RISC-V MCU family also expands quickly. 
CH32V003 is an ultra low cost entry level RISC-V MCU suitable for small products or projects.
It features:

- 32bit RISC-V 2A Core
- Up to 48MHz system clock
- 2KB SRAM and 16KB user flash
- 1x DMA controller
- 1x OpAmp comparator
- 10-bit ADC
- 1x 16-bit advanced timer and 1x 16-bit general purpose timer
- 2x watchdog timers and 1x 32-bit system-tick timer
- 1x USART
- 1x I2C
- 1x SPI interface
- Up to 18 I/O ports, interrupt is available on all pins
- 64-bit chip unique ID
- Serial 1-Wire Debug Interface
- Multiple low power modes: sleep, standby
- Supply voltage: 3.3/5V
- Power-on/power-off reset, programmable voltage detector

## An Awesome Low Pin Count MCU for DIYer

Why TSSOP20 is perfect for DIYer? Because it is the most user-friendly package for DIYer.
NXP used to offer DIP20 of LPC MCU for the DIYer. But DIP was too expensive. That is the
reason why new boards including Arduino Nano/Raspberry Pi Zero leverage 100mil DIP pinout, 
with QFP/QFN package on board.

Compare to QFN, TSSOP20 is easier to handle.

## Revised User Friendly EVB

WCH has it own EVT (Evaluation Tool), which has simpler design and low cost. The Revised EVB features:

- One through-hole external crystal soldering pad to use low cost 8MHz crystal.
- USB Type-C connector offers 5V power supply
- On board USB/UART bridge IC to print out the debug information via serial port.
- One power switch
- One PWR LED
- Two user LEDs
- Two user buttons
- One reset button
- WCH SDI debug interface

The design adds USB/UART and user buttons/LEDs for user interactions. So only two USB ports
are used during development: PWR+DBG. Or you can use WCH-LINK-E to support power, UART and DBG.

For people who still wants the official EVT from WCH, please visit our store for detail.

## Revised WCH-LINK-E

WCH offers various WCH-LINK debuggers, since the company offers ARM and RISC-V MCUs. 
Only WCH-LINK-E is suitable for CH32V003 and newer RISC-V microcontrollers. The interface
also supports Keil and ARM Cortex-M microcontrollers from WCH.

This version of WCH-LINK-E is a clone and has been verified for compatibility.

We also offer the official version, please visit our store for detail.

## Samples

The blank samples are suitable for the developers who wants to design their own boards.
CH32V003 makes its bootloader area available for developers who want a custom bootloader or
use all the memory completely. 

The developer has to program the chip with WCH-LINK-E at first time. An open source bootloader 
is also available in the software library.

The samples have to be ordered seperately. The packages/quantities can be arranged on request.
