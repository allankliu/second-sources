# RISC-V CH32V003 Official EVB Kit (<50 chars)

## Layman Description (<140 chars)

A complete development kit for low cost RISC-V MCU CH32V003 with optional debugger, samples and documents

## Product Description (>250 chars)


## Known Restrictions, First Thing First

- CH32V003 instruction set is optimized for control applications, instead of computation tasks.
- CH32V003 blank chips are shipped without bootloader, use WCH serial debug interface (SDI) debugger to download.
- CH32V003 blank chips can only be programmed by WCH-LINK-E with SDI.
- All documents in English are proofread for global developers.
- More restrictions will be collected and appended here.

## A Low Cost RISC-V MCU

WCH is well-known for its USB interface IC family. Its RISC-V MCU family also expands quickly. 
CH32V003 is an ultra low cost entry level RISC-V MCU suitable for small products or projects.
It features:

- 32bit RISC-V 2A Core
- Up to 48MHz system clock
- 2KB SRAM and 16KB user flash
- 1x DMA controller
- 1x OpAmp comparator
- 10-bit ADC
- 1x 16-bit advanced timer and 1x 16-bit general purpose timer
- 2x watchdog timers and 1x 32-bit system-tick timer
- 1x USART
- 1x I2C
- 1x SPI interface
- Up to 18 I/O ports, interrupt is available on all pins
- 64-bit chip unique ID
- Serial 1-Wire Debug Interface
- Multiple low power modes: sleep, standby
- Supply voltage: 3.3/5V
- Power-on/power-off reset, programmable voltage detector

## An Awesome Low Pin Count MCU for DIYer

Why TSSOP20 is perfect for DIYer? Because it is the most user-friendly package for DIYer.
NXP used to offer DIP20 of LPC MCU for the DIYer. But DIP was too expensive. That is the
reason why new boards including Arduino Nano/Raspberry Pi Zero leverage 100mil DIP pinout, 
with QFP/QFN package on board.

Compare to QFN, TSSOP20 is easier to handle. 

## Official EVB from WCH

WCH call EVB as EVT (Evaluation Tool), the schematics is available in Github/Gitlab.

It features:
- One external 24MHz crystal
- USB Type-C connector offers 5V power supply
- One power switch
- One PWR LED
- Two user LEDs
- One reset button

It is a simple board with MCU and minimal power supply, you have to use external USB/UART
bridge if you want to see the debug information. Usually you need three USB ports for 
development: PWR/UART/DBG. But you can also use WCH-LINK-E to offer such interfaces with one
USB port. Besides the physical UART of CH32V003, you can also use SDI_prinf to print 
the debug information via SDI.

However, you have to wire out for LED and user buttons anyway.

We also offer other versions with on board USB/UART bridge IC and user LEDs/buttons, please 
visit our store for detail.

## Official WCH-LINK-E

WCH offers various WCH-LINK debuggers, since the company offers both ARM and RISC-V MCUs. 
Only WCH-LINK-E is suitable for CH32V003 and newer RISC-V microcontrollers. The interface
also supports Keil and ARM Cortex-M microcontrollers from WCH.

We also offer other clone versions of WCH-LINK-E, please visit our store for detail.

## Samples

The blank samples are suitable for the developers who wants to design their own boards.
CH32V003 makes its bootloader area available for developers who want a custom bootloader or
use all the memory completely. 

The developer has to program the chip with WCH-LINK-E at first time. An open source bootloader 
is also available in the software library.

More samples in other packages/quantities can be arranged on request.
