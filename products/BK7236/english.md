## Brief

The BK7236 is a highly-integrated 1x1 single-band 2.4 GHz Wi-Fi 6 (802.11b/g/n/ax) and Bluetooth 5.4 combo solution designed for applications that require high security and abundant resources. The integration of dual-core 32-bit ARMv8-M Star M33 MCU and comprehensive set of peripherals makes the BK7236 ideal for advanced Internet of Things (IoT) applications.

The BK7236 provides state-of-the-art security based on a powerful security architecture. The BK7236 integrates a IoT Platform Security Suite (IPSS) for cryptograph and system security control. The IPSS embeds all-round and robust security features to set up a top-secret execution environment for IoT devices.

Using advanced design techniques and ultra-low-power process technology, the BK7236 delivers high integration, efficient security and minimal power consumption for a wide range of complex IoT applications. 

## Key Features

### WiFi 

• IEEE 802.11b/g/n/ax 1x1 compliant
• 20/40 MHz channel bandwidth for 2.4 GHz
• Supports downlink Multi-User Multiple-Input Multiple-Output (DL MU-MIMO)
• Supports uplink Orthogonal Frequency Division Multiple Access (UL OFDMA)
• Supports individual Target Wake Time (iTWT)
• TX and RX low-density parity check (LDPC) support for extended range
• WPA/WPA2/WPA3-Personal support for enhanced security
• Working mode: STA, soft AP, Direct
• Concurrent AP + STA
• Integrated Bluetooth/WLAN coexistence (PTA)
• TX power up to +21 dBm
• RX sensitivity -99 dBm

### BLE

• Dual-mode Bluetooth 5.4 compliant
• Supports Basic Rate (BR), Enhanced Data Rate (EDR) 2 Mbps and 3 Mbps, Low Energy (LE) 1 Mbps, 2 Mbps, and long range (125 kbps and 500 kbps)
• Advertising extensions
• Bluetooth direction finding: Angle of Arrival (AoA) and Angle of Departure (AoD)
• Supports an antenna array with up to sixteen antennae for precise indoor positioning

### Core

• Dual-core ARMv8-M Star M33 MCU:
  - One low power 120 MHz and one high performance 320 MHz
  - Each core features a double-precision floating point unit (FPU)
  - Each core has 16 K ITCM + 16 K DTCM
  - Each core embeds TrustZone
  - Each core supports DSP instructions with SIMD
• EEMBC CoreMark® score: 3.84 CoreMark/MHz
• SPI/UART for Flash download
• Serial wire debug (SWD) interface

### Memory

• SiP Flash: 4 MB or 8 MB
• SiP PSRAM: 4 MB or none
• 512 KB Share SRAM
• 128 KB ROM
• 32-bit eFuse
• 32 Kbit PUFrt

### Security

The BK7236 integrates the IoT Platform Security Suite (IPSS) for cryptograph and system security control. The key features of the IPSS include:

• Secure boot
• Secure debug
• Secure connection
• Firmware Over-The-Air (FOTA)
• Provisioning
• TEE_M
• TrustEngine, which includes the following features:
  - Symmetric schemes, AES-ECB/CBC/CTR/CBC-MAC/CMAC/CCM/GCM (key size 128-bit, 192-bit and 256-bit)
  - Symmetric schemes, SM4-ECB/CBC/CTR/CBC-MAC/CMAC/CCM/GCM
  - Digest schemes, SHA1/224/256
  - Digest scheme, SM3
  - Asymmetric schemes, RSA 1024/2048/3072/4096 and ECCP 192/224/256/384/512/521
  - Asymmetric scheme, SM2
  - Key ladder for key management
  - Lifecycle management
  - True random number generator

### Clock

• External oscillator: 26 MHz crystal oscillator (X26M), 32.768 kHz crystal oscillator (X32K)
• Internal oscillator: 32 kHz ring oscillator (ROSC), 26 ~ 240 MHz digitally controlled oscillator (DCO)
• 320/480MHz PLL (DPLL)

### Power Management

• 1.7 to 5.5 V VBAT supply
• On-chip power-on reset (POR) and brown-out detector (BOD)
• Embedded buck (DC-DC) converters and LDO regulators
• Low power consumption:
  - Active mode RX: 15 mA
  - Low voltage standby mode: 50 μA
  - Deep sleep mode: 5.0 μA
  - Shutdown mode: 1.0 μA

### Peripherals

• 20 GPIOs, configurable 3.0 V/5.0 V voltage
• 1x SPI
• 3x UART, 1 with hardware flow control and flash download support
• 1x SD/SDIO
• 2x I2C
• 1x high-speed USB (HS)
• 2x general-purpose DMA controller (GDMA), each with 8 channels
• 6x 32-bit PWM
• 1x 15-bit SAR ADC, 4 channels
• 2x 32-bit general-purpose timer/counter
• 2x watchdog
• 1x real-time counter (RTC)
• 1x temperature sensor
• 1x touch sensor

### Packaging

• QFN40 package, 5 x 5 mm
• Operating temperature range: -40 up to +125 °C

### Target Applications

- Smart appliances
- Smart plug
- Smart lighting
- Door locks