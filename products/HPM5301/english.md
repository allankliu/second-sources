# HPM5301 RISC-V High Performance MCU

## Features

- 360MHz RISC-V
- 288KB SRAM
- 1MB flash memory
- USB PHY for USB OTG
- 20 pin JTAG
- Type-C USB connector
- Key pad
- LED
