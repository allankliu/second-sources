HPM6750 is a high performance dual-core RISC-V MCU running on 800MHz. HPMicro licensed the RISC-V cores from Andes Technologies. The chip is designed for high-end IoT/edge computing and embedded systems. It can be a good replacement to Cortex-M7 micros such as STM32H750.

## Core

Dual RV32-IMAFDCP by Andes
DSP with SIMD/DSP instructions
L1 32KB instruction cache and 32KB data cache
256KB ILM/DLM 

## Memory

Up to 2MB on-chip SRAM
4096bit OTP
128KB BOOT ROM

## Power and Clock

Multiple on-chip power supply, including DC/DC and LDO
Low power modes: run/wait/stop/hibernate and shutdown
24MHz / 32.768KHz crystals
5x PLL, support fraction division

## External Memory Interface

2x serial bus controller XPI, support serial flash and PSRAM
1x DRAM controller, support 8/16/32bit SDRAM/LP SDRAM on 166MHz
2x SD/eMMC controllers, support SD/SDHC/SDXC, eMMC5.1

## Image Subsystem

24bit RGB display interface
2x DVP camera interfaces
2D graphic accelerator
JPEG codec

## Audio Subsystem

4x I2S interfaces
PDM digital microphones
digital audio output
audio detection module

## Motor Subsystem

4x PWM timers, in 2.5ns resolution
4x QDEC and 4x HALL sensor interface

## Timers

9x 32bit GP Timers
5x WDG timers
RTC with calendar

## Communications

17x USART, 4x SPI, 4x I2C
2x USB 2.0 OTG, with HS-PHY
2x 1G Ethernet controller
4x CAN controller, CAN-FD

## High Performance Analog Peripheral

3x 12bit ADCs, on 5Msps
1x 16bit ADC, on 2Msps
4x analog comparators

## IO

195x GPIO
IO operation on 3.3V/1.8V

## Security

AES-128/256 encryption for ECB/CBC
SM2/SM3/SM4
SHA-1/256 hash
TRNG
NOR Flash decryption in realtime

