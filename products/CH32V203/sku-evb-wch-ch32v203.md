# RISC-V CH32V203 Official EVB Kit

## Layman Description 

A development kit for low cost mid-end RISC-V MCU CH32V203 with optional debugger, samples and documents.

## Product Description

## Known Restrictions, First Thing First

- No restriction for the MCU has been reported yet.
- CH32V203 is based upon QinKe V4B core, aka a RV32IMAC derivate.
- CH32V203C8T6 on board, QFP48, 64KB flash and 20KB SRAM, 37x GPIO

## A Enhanced RISC-V MCU

## A Powerful MCU for DIYer

## Official EVB from WCH

## WCH-LINK-E Debugger Interface

## Samples



