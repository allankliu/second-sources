The CH32V203 is a low-pin count RISC-V microcontroller based upon RISC-V4B core.
The key features are:

- Qing Ke RISC-V4B core, 
- FPIC + hardware interrupt stack
- single cycle multiplication/division intructions
- Running on 144MHz
- up to 64KB NV data memory SRAM
- up to 224KB CodeFlash (zero waiting and non-zero waiting)
- 28KB Bootloader
- 128B system configuration information memory
- 128B user defined information memory
- Multiple LPM modes with independent RTC and backup power supply
- 8MHz/40KHz IRC 
- PLL up to 144MHz
- 3~25MHz or 32.768KHz XTAL
- POR/PD reset and programmable voltage surveillance
- RTC independent 32bit timer
- 1x 8 channel GP DMA controller with ring buffer management
- DMA supported TIMx/ADC/USART/I2C/SPI
- 2x OpAmp and Comp, connects to ADC/TIMx
- 2x 12bit ADC with 16 external inputs and 2 internal inputs
- ADC with onchip temperature sensor
- 16 channel touch key detector
- Multiple timers: 1x advanced, 3x basic, 1x 32bit GP and 2x IWDT, 64bit systicker
- 4x USART
- 2x I2C (SMBus/PMBus)
- 2x SPI
- USB2.0 full speed device/host interface
- 1x CAN Bus (2.0B active)
- 37x GPIO with 16 mapped external interrupts
- Security with CRC unit and 96bit Chip UID
- Debug interface: two wire interface
- Package: LQFP, QFN, TSSOP and QSOP
- More

Since it also offers TSSOP package, it is suitable to develop high performance
FOC motor drivers. It is easy to handle and replace. 

## Highlight

- Enhanced on caculation operation intructions.
- OpAmp/Comp on chip, which is not common in a low cost micro.
- Dual ADC on chip, 12bit is good enough for industrial applications.
- USB host/device on chip, makes it easy to connect to PC or mobile.
- Fast enough and good memory configuration for connected IoT and embedded AI applications.
- A complete SDK called EVT is available from the official web.

