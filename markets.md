## Market Segments

The product lines of semiconductors have been moved to productlines.md. 

The market is oriented to the users' application markets.

- IC prototypes (FPGA/SDR)
- HPC and AI
- New engergy and Opto-Volt
- Consumer Video
- Consumer Audio
- Consumer Wearables
- Health Management
- Telecommunications
- Automotives and EV
- Robotics (UAV/USV)
- STEM education
- Smart city
- Smart facility & Industrial
- Smart farm & agriculture
- Commodities and General Purposes IC

## Vertical Markets

- Opto-Volt new energy industry (PV), networking, communication, inverters, batteries, opto-electronics modules, motors and more.
- Automotives, including networking, commnications, V2X, inverters, batteries, AI, GPU, motors and more.
- IT and HPC, including FPGA, display, power management, AI, GPU, networking and more.
- Telecommunications, 5G mobile phones, base stations, switches and opto-fibers, radio.
- Health care devices, including hospital oriented and health care devices.
- Assembly modules, including RF and sensor modules.
- Industrial applications, DAQ and specific applications
- Transportations, including V2X.

## STEM Applications

- Optical, camera for industrial and security, timelapse
- Audio, noise source positioning and analytics.
- Motor drivers, in robotics, 3D printing and more.
- Music instructments, by MIDI
- Wearables, for sports and backpacks.
- Gardening, for watering and more.
- Games peripherals
- Solar power, inverter, batteries and more.
- Radio, AM/FM radio and hacks.
- Health care, cycling meters, paddling.
- Sports, outdoor tracking, positioning.


### To Reduce the Cost of Customization

- Customization for a specific application for a specific client has very high cost.
- Using standardized hardware platform to reduce chip level cost.
- Using standardized firmware platform to reduce firmware development cost.
- Using standardized board footprint to reduce board level and device level cost.
- Using standardized cloud services to reduce system level cost.
- Using SiP to reduce IC level cost in mass production.