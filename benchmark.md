## benchmark

In order to test the performance of selected chip, we will test the following conditions:

- CPU performance (run in RAM)
- Memory types　(to judge whether embedded flash or SPI flash is used.)
- ADC performance
- PWM performance
- GPIO performance
- Interrupt performance
- RTOS performance
- Security performance

The above benchmark will be integrated with EEMBC CoreMark test suites.

- https://github.com/eembc/coremark
- https://github.com/eembc/coremark-pro
- https://github.com/eembc/securemark-tls
- https://github.com/eembc/telebench