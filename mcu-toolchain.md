## MCU Toolchain

| Family    | Core     | ROM     | RAM    | OS      | Driver | Probe  | Debugger | Programmer    | ISProgrammer | PlatformIO | Arduino | Memo
|-----------|----------|---------|--------|---------|--------|--------|----------|---------------|--------------|------------|---------|-----
| STM32F103 |Cortex-M3 | 64K     | 16K    | Windows | CubeX  | STLINK | Keil/IAR | CubeX         | STM32loader  | Yes        | Yes     | Benchmark
| STM32F401 |Cortex-M4 | 256K    | 32K    | Windows | CubeX  | STLINK | Keil/IAR | OpenOCD/JLINK | STM32loader  | Yes        | Yes     | Benchmark
| LPC8XX    |Cortex-M0 | 16K     | 2K     | Windows | SDK    | JLINK  | Keil/IAR |               | LPC
| ESP32C3   |RISC-V    |         |        |Linux   | IDF    | JLINK  |
| W801      |CSKY-E802 |         |        |Linux   | SDK    |USB/UART| GCC      |               | WXX python loader
| W806      |CSKY-E804 |         |        |Linux   | SDK    |USB/UART| GCC      |               | WXX python loader
| HC32L110  |Cortex-M0 | 16K     | 2K     |Windows | HAL    | JLINK  | Keil/IAR | JLINK         | 
| HC32L110  |Cortex-M0 | 16K     | 2K     |Linux   | HAL    | JLINK  | GCC      | pyOCD/JLINK   |
| HC32L110x4|Cortex-M0 | 16K     | 2K     |Linux   | HAL    | JLINK  | GCC      | pyOCD/JLINK   |              |            |         | 16K
| HC32L110x6|Cortex-M0 | 16K     | 2K     |Linux   | HAL    | JLINK  | GCC      | pyOCD/JLINK   |              |            |         | 32K 
| HC32L15   |Cortex-M0 |128K
| HC32M120  |
| HC32F003  |Cortex-M0 |16K
| HC32F005  |Cortex-M0 |32K
| HC32F15   |Cortex-M0 |64K
| HC32FM14  |128K
| HC32HC17X |128K
| HC32HC19X |256K
| HC32F030  |64K
| HC32F072  |128K
| HC32L07X  |128K
| HC32L17X  |128K
| HC32L19X  |256K
| HC32L130  |64K
| HC32L136  |64K
| PHY6220   |Cortex-M0 |
| PHY6222   |CSKY-E802 |
| N32G4FR   |Cortex-M4 |
| AT32F413  |Cortex-M4 |
| GD32F103  |Cortex-M3 |
| GD32V103  |RISC-V    |
| GD32E230  |Cortex-M4 |
| CH32V103  |RISC-V    |
| CH32V307  |RISC-V    |
| PY32F003  |Cortex-M0 |
| PY32F030  |Cortex-M0 |
| K210      |RISC-V    |
| RP2040    |Cortex-M0 |
| HP6450    |RISC-V    | Linux   |        | JLINK  | GCC
| ES32F369  |Cortex-M3 | Windows | SDK    | JLINK  | Keil/IAR |


Q: How can we use AI to speed up the evaluation and porting support for the embedded controllers?
