## Candidate Product Lines

- https://zhuanlan.zhihu.com/p/439315671
- https://www.zhihu.com/people/gu-zheng-shu
- https://www.eefocus.com/article/526428.html

The ICs can also be divided into the following sectors and sub-sectors:

- Analog
    - PMIC/PMU/BMU
    - Signal chain/ADC/DAC/OpAmp/Comparator
    - RFIC/RFSoC
    - Interface/Telecom modem
- Logic
    - FPGA/CPLD/FPSoC
    - NPU
    - Glue logic
- Memory
    - NOR/NAND Flash
    - DRAM/SRAM/PSRAM
    - FeRAM/ReRAM/EEPROM
    - RFID/NFC
- Micro
    - MPU/APSoC
    - [x] MCU/DSC/PSoC

### Components

- OptoElec/Display
- MEMS Sensors
- SBC Core Module
- RF Module

--------------------------------------

### Analog/PMIC

https://www.eet-china.com/news/202102191218.html

PMIC/AC-DC/DC-DC/AC-AC/LDO/BMU/LED driver/Voltage Reference/Supervisory/USB interface/PoE/GaN/Charger

- RK805, PMIC for APSoC from Rockchip
- AXP209, PMIC for APSoC from Allwinner
- CH43X, USB communication interface from WCH, Hengxing

### Analog/Signal Chain

- https://tech.hqew.com/news_2060512

ADC/DAC/Amplifier/Comparator/Audio/Signal conditioning, interface and isolator

- SGM620, alertnative to AD620, amplifier from SGM
- TPAFE0808, 8x 12b ADC and 8x 12b DAC, from 3PEAK
- CA-IS372x, isolator IC from Chuan Tu Micro
- AW88195CSR, digital audio amplifier with I2S/TDM from Ai Wei
- SIA810x, Analog inputs class-D digital audio amplifier 
- CL3669, pipeline ADC, alternative to ADS42LB69, dual channel 16bit 250Msps ADC from hexin
- CL4651, DAC, alternative to DAC80501, 16bit DAC from hexin
- GP8202S 12b DAC to 4~20mA
- CBM08AD1500QP, dual channel 8bit ADC, 1.5GSPS from Xin Bai Wei (CoreBai)
- AAD08Q2500, quad channel 8bit ADC, 10GSPS from Xun Xin Wei
- CMS24AD, 24bit ADC MCU SoC
- CS1242, 24bit ADC from ChipSea
- CS1232, ENOB 23.5bit ADC from ChipSea
- CS32A060, ChipSea Cortex-M0 24bit ADC SoC
- ZML165, ZLG 24bit ADC 48MHz Cortex-M0 SoC
- ZML166, ZLG 24bit ADC 48MHz Cortex-M0 SoC

### Analog/RFIC
RFIC/LNA/PA/Duplexer/Tuner/RF switch

- LT8910, 2.4GHz RFIC
- CTM2300, Sub-1G FSK transceiver, alternative to SLE44XX, from HopeRF
- HW3000, Sub-1G FSK transceiver, 20dB TX -114dBm RX, from EastSoft
- TLSR8359, Telink 2.4GHz/Sub-1GHz Propietary RF SoC (IoT)
- A8125, AMICCOM 2.4GHz/Sub-1GHz Propietary RF SoC (toy/mouse)
- A8127, AMICCOM RF SoC
- PAN159CY, 2.4GHz RF SoC
- Si24R1, 2.4GHz transceiver, alternative to nRF24L01+
- Ci24R1, 2.4GHz transceiver, alternative to nRF24L01
- BK2421, 2.4GHz transceiver, alternative to nRF24L01
- SE8R1, 2.4GHz transceiver, alternative to nRF24L01
- Si3933, 125Khz SoC,replace AS3933
- Ci2454, 2.4GHz 8bit SoC
- BL618, bouffalolab WiFi6/BT/ZigBee 320MHz RISC-V SoC
- BL706, bouffalolab 144MHz RISC-V BLE/ZigBee SoC
- CW32W031, 48MHz Cortex-M0 Sub-1G ChirpIoT SoC (370~590/740~1180MHz), similiar to LoRa Chirp modem
- ESP32-C6, RISC-V, WiFi/BLE/IEEE802.15.4
- TK8610, RISC-V TX:20dB, RX:-141dBm 150~960MHz TRUMASS Taolink
- GPS/BD/GNSS IC and module

--------------------------------------

### Logic/FPGA/CPLD

- [GoAI 2.0](http://www.gowinsemi.com.cn/solution_view.aspx?FId=n25:25:25&Id=577)

- GW2A, GW1N, GoAI and BT/FPGA SoC, Gowin, FPGA/SoC (Display/Telecom/AI/CE)
- Phoenix, Eagle, Elf, Anlogic, FPGA/SoC (Display/Telecom/Industrial)
- Titan, Logos, Compact(CPLD), Guoxin, FPGA/Secure (PMIC/5G)
- AiPGA, HPA, eFPGA, FX, Hercules Micro, FPGA (Industrial/AI)
- HqFPGA, Seagull1000 CPLD, Sealion2000 and Seal5000 FPGA, Intelligence Silicon, CPLD/FPGA (Display)
- SinoMicro Electronics, ADC/DAC, CPLD/FPGA
- PSoC, AGM Micro, CPLD/FPGA
- FPGA, Fudan Microelectronics

- GW5A/GW5AT/GW5AST, FPGA, MIPI, DSP, SDRAM, Arora V series from Gowin
- GW2A/GW2AR/GW2AN/GW2ANR, FPGA, LVDS, DSP, SRAM, Arora series from Gowin
- GW1N/GW1NR/GW1, FPGA MIPI, DSP, 55nm eFlash, auto boot, dual boot
- GW1NSE, FPGA, Cortex-M3, DSP, SRAM BUF, 55nm eFlash, security module
- GW1NRF, FPGA, ARC, ROM and OTP, BLE, 28KDRAM (similiar to Dialog BLE?)
- GoAI 2.0, for image, video, audio
- SAL-EAGLE3, 23.4K LUTs, 64Mb/128Mb SDRAM
- SAL-AL3, 8640 LUTs, 64Mb SDRAM
- SAL-ELF2, 4500 LUTs, flash and 64Mb PSRAM
- SAL-ELF3, 11776 LUTs, 8Mb flash
- SAL-EF3L15, 4480 LUTs, 4Mb flash
- SAL-SWIFT1, 6K LUTs, RISC-V, MIPI DSI DPHY, flash and 128Mb PSRAM
- SAL-PHOENIX 1A, 180K LUTs, SERDES, 2.5Gbps MIPI, 1866Mbps DDR4, PCI Express
- AG256SL100, CPLD, LQFP-100, 256 LEs, 256Kbits flash, compatible to EPMXXXTXXXxN, from AGM
- AGRV2K100, PSoC, CPLD, LQFP-100, 2K LEs, RISC-V, compatible to EPM1270, from AGM
- AG1KLPQ48, FPGA, QFN-48, 1280 LEs, from AGM
- AG10KLXXX, FPGA, compatible to EP3C, from AGM
- FPSoC SF1, FPGA, RISC-V from Anlogic
- Seal5000 5A5Z-30-D1-8U213 SoC FPGA, 168MHz Cortex-M3, 30K LEs, 128Mbit DDR2, 2x 12b ADC. Replacement of Xilinx Atrix-7/Altera EP4CE

--------------------------------------

### Memory

- SPI NOR flash, GigaDevices
- Flash interface IC, Montage Technology
- NAND/NOR flash/MCU, XTX Technology
- NAND/NOR/DRAM/MCP, Dosilicon
- EEPROM/ICC, Giantec Semicon
- Flash controller IC, Yeestor
- SPI NOR flash, Zbit Semicon
- SPI NOR flash, Puya Semicon

--------------------------------------

### Micro/MCU/SoC

Based upon ARM Cortex-M/Cortex-A, RISC-V, MIPS/Loong, M-Core/C-Sky, x86, 8051, AVR, ARC, Xtensa and etc.

#### Github

- https://github.com/orgs/SoCXin/repositories

The following repos have been re-ordered. The leading companies of MCU/SoC are:
Artery, Bouffalo, Beken, ChipSea, WCH, EastSoft, GD, HDSC/XHSC, MindMotion, Nations, Synwit, Telink, PhyPlus.

The products covers 
- ARM Cortex-M based MCU, 
- ARM Cortex-A based MPU, 
- RISC-V (32/64bit) based MCU and MPU.
- M-Core/C-Core/C-Sky V2 
- MIPS/Loongson based MCU and MPU.
- FPGA with low/medium density
- IoT SoC with Sub-1G/Lora/802.15.4/BT/BLE/WiFi/NFC/NB-IoT/CAT-1
- ASIC SoC with ETC/PLC/Daolink/Energy Metering/FingerSensor/DRAM

Candidates:

- 1C300B, Loongson 240MHz LoongArch MCU
- 2K1000, loongson 1.0GHz LoongArch CPU (2K1000)
- AB32VG1, Bluetrum RISC-V 120MHz
- AC7801, Autochips Cortex-M0 MCU
- AC7815, Autochips Cortex-M3 100MHz MCU
- AC6321A, JieLi 96MHz RISC-V BT SoC
- AC7916, JieLi 320MHz RISC-V Dual Core WiFi/BLE SoC
- AG32VF407, 248MHz RISC-V with 2K CPLD, PSoC
- AGRV2KXXX, 2K CPLD (LEs) with RISC-V MCU
- AIC8800M, RISC-V WiFi/BT MCU
- AIR105, luatos 192MHz Cortex-M4F MCU 
- AIR32F103, luatos 216MHz Cortex-M3 MCU
- AT32F403A, Arterytek Cortex-M4 240MHz
- AT32F407, Arterytek Cortex-M4 240MHz
- AT32F413, Arterytek Cortex-M4 200MHz
- AT32F415, Arterytek Cortex-M4 150MHz
- AT32F421, Artery Technologies 120MHz Cortex-M4 MCU
- AT32F437, arterytek 288MHz Cortex-M4 MCU
- APT32F003, C-Sky MCU
- APT32F172, C-Sky MCU
- ASR1601, ASR Cortex-R5 LTE Cat.1 SoC
- ASR1606, ASR 624MHz Cortex-R5 Cat.1 SoC
- ASR1802, Cellular chip with LPDDR
- ASR6501, Cortex-M0 LoRa SoC
- BAT32WB35, Cmsemicon 48MHz Cortex-M0 BLE SoC
- BF5823, BYD 12MHz MCS-51 RFID SoC
- BF5885AM64, BYD 48MHz Cortex-M0 RFID SoC
- BK5881, Beken ETC/BLE SoC
- BK7231, Beken WiFi/BLE SoC
- BK7238, Beken WiFi/BLE SoC
- BL602, bouffalolab 192MHz RISC-V Wi-Fi/BLE SoC
- BL618, bouffalolab WiFi6/BT/ZigBee 320MHz RISC-V SoC
- BL706, bouffalolab 144MHz RISC-V BLE/ZigBee SoC
- BL808, WiFi/BLE RISC-V SoC
- CH32F103, WCH Cortex-M3 MCU
- CH32F207, WCH Cortex-M3 MCU
- CH32F208, WCH 144MHz Cortex-M3
- CH32V003, WCH 48MHz RISC-V MCU
- CH32V203, WCH 144MHz RISC-V4B MCU
- CH32V208, WCH 144MHz RISC-V4C BLE SoC
- CH32V307, WCH RISC-V MCU
- CH543, WCH 1T 8051 USB PD MCU 
- CH545, WCH 8051 USB HUB MCU
- CH549, WCH 8051 USB PD MCU
- CH565, WCH 120MHz RISC-V3A
- CH569, WCH 120MHz RISC-V3A USB3.0 SoC
- CH573, WCH RISC-V BLE SoC
- CH579, WCH Cortex-M0 BLE SoC
- CH583, WCH 80MHz RISC-V4A BLE SoC
- CH592, WCH 20MHz RISC-V4C BLE SoC
- CKS32F030, Cortex-M0 MCU from CETC
- CKS32F103, Cortex-M3 MCU from CETC
- CKS32F407, Cortex-M4 MCU from CETC
- CR600, Cortex-M0 PLC SoC
- CR710 ARM946ES PLC SoC
- CS32A039, ChipSea Cortex-M0 MCU
- CS32A060, ChipSea Cortex-M0 24bit ADC SoC
- CS32G020, ChipSea Cortex-M0 PD MCU
- CS32L010, ChipSea 24MHz Cortex-M0 BLE MCU
- CSM32RV20, CSM-IC RISC-V MCU
- CW32F030, 64MHz Cortex-M0 MCU
- CW32W031, 48MHz Cortex-M0 Sub-1G ChirpIoT SoC (370~590/740~1180MHz), similiar to LoRa Chirp modem
- CW3802, RISC 80MHz MCU
- EAI80, Gree Edgeless Cortex-M4F SoC
- EC616, eigencomm 204MHz Cortex-M3 NB-IoT SoC
- EC618, eigencomm 204MHz Cortex-M3 Cat.1 SoC 
- EFM32MG21, multi-mode 2.4GHz SoC from Silabs
- EG4S20, Anlogic FPGA with SDRAM in SiP
- ES32F027, Eastsoft Cortex-M0 MCU
- ES8P508, EastSoft Cortex-M0 48MHz
- ESEM16, Eastsoft Cortex-M0 Energy Meter SoC
- ESP32-S3, Espressif 240MHz Xtensa Wi-Fi/BLE SoC
- ESP32-C3, Espressif 160MHz RISC-V Wi-Fi/BLE SoC
- ESP32-P4, Expressif 400MHz RISC-V MCU
- F133, RISC-V D1S
- FE310, SiFive RISC-V 320MHz SoC
- FM33LC0, Fudan Cortex-M0
- FT60F210, FremontMicro 8bit RISC MCU
- FR8016A, FreqChip 48MHz BLE SoC
- GD32A502, GigaDevice 100MHz Cortex-M33 Car MCU
- GD32E103, GigaDevice Cortex-M4 120MHz MCU
- GD32E231, GigaDevice Cortex-M23 72MHz MCU
- GD32F103, GigaDevices 108MHz Cortex-M0 MCU
- GD32F150, GigaDevice Cortex-M3
- GD32F303, GigaDevice Cortex-M4
- GD32F350, GigaDevice Cortex-M4 108MHz
- GD32F450, GigaDevice Cortex-M4 200MHz
- GD32FFPR, GigaDevices Cortex-M4 168MHz MCU
- GD32W515, GigaDevice 180MHz Cortex-M33 WiFi SoC
- GT1000, UWB SoC
- GW1NSR, Gowin LittleBee FPGA SoC
- HC32D391, XHSC 200MHz Cortex-M4F MCU
- HC32F005, HDSC Cortex-M0 MCU
- HC32F072, HDSC Cortex-M0 MCU
- HC32F4A0, XHSC 240MHz Cortex-M4 MCU
- HC32F460, XHSC 200MHz Cortex-M4 MCU
- HC32L072, HDSC Cortex-M0 48MHz MCU
- HC32L110, XHSC 32MHz Cortex-M0 LE MCU
- HC32L136, HDSC Cortex-M0 48MHz MCU
- HC32M120, HDSC Cortex-M0 48MHz MCU
- HCM3043, XHSC 48MHz Cortex-M0 Motor MCU
- Hi3861, Huawei WiFi SoC
- HK32AUTO39A, HK 120MHz Cortex-M3 SoC
- HK32F030M, HK 32MHz Cortex-M0 MCU
- HPM6750, HPMicro 816MHz RISC-V Dual Core SoC
- HR8P506, Eastsoft 48MHz Cortex-M0 MCU
- HT5027, Hitrend Tech Cortex-M0 SoC
- HT8550, Hitrend Tech 8051 G3&BPSK PLC SoC
- HT8922, Hitrend Tech Cortex-M0 G3/BPSK PLC SoC
- HW3181, EastSoft Cortex-M0 SoC
- K210 RISC-V 400MHz Dual Core AIoT 0.2TOPS SoC
- K510 RISC-V 800MHz Dual Core AIoT 2.5TOPS SoC
- KF32A141, Chipon 48MHz MCU
- LGT8F328P, LogicGreen AVR 32MHz
- LPC824NFC, NFC with Cortex-M0 from NXP
- MH32F103A, XinLingGo 216MHz Cortex-M4
- MM32SPINEBK, mindmotion 96MHz Cortex-M0 Motor MCU
- MM32SPIN05, MindMotion 72MHz Cortex-M0
- MM32SPIN422C, mindmotion Cortex-M0 Motor MCU
- MM32SPIN580C, mindmotion Cortex-M0 Motor MCU
- MM32F013, MindMotion Cortex-M0
- MM32F031, MindMotion Cortex-M0
- MM32F103, MindMotion Cortex-M3 144MHz
- MM32L062, MindMotion 48MHz Cortex M0 MCU
- MM32W073, MindMotion 48MHz Cortex M0
- N32G430, Nations Tech 128MHz Cortex-M4F ADC MCU
- N32G435, Nations Tech 108MHz Cortex-M4F ADC
- NG32G455, Cortex-M4 144MHz MCU
- NS32G020, Nation Tech Cortex-M0 80MHz MCU
- PHY6220, Phyplus C-Sky V2 BLE SoC
- PHY6222, Phyplus 96MHz Cortex-M0 BLE SoC
- PL51RC003, PMicro 12MHz 8051 RFID SoC
- PY32F002, PuyaSemi 24MHz Cortex-M0 MCU
- RDA5981, Cortex-M4 160MHz WiFi SoC
- RN8613, Cortex-M0 Energy Meter MCU
- RPC801, rpcom Cat.1 SoC
- RT1062, Cortex-M7 RT MCU 600MHz
- RTL8720, Realtek WiFi/BLE SoC
- SC32F53, Cortex-M0
- SH32F284, Cortex-M3 84MHz MCU
- SH32F205, Cortex-M3 120MHz MCU
- SPC7L64, Cortex-M0 for Motor Control
- SSD201, Cortex-A7 SigmaStar
- STM32G474, ST Cortex-M4 170MHz
- STM32WLE5, ST Cortex-M4 LoRa SoC
- STC32M4, STC Cortex-M4 MCU
- SWM050, Synwit 50MHz Cortex-M0
- SWM181, Synwit 48MHz Cortex-M0 SD-ADC MCU
- SWM320, Synwit 120MHz Cortex-M4 MCU
- SWM34S, Synwit 150MHz Cortex-M33 LCD MCU
- TKM32F499, tiky Cortex-M4 240MHz MCU
- TLSR8258, Telink 48MHz BLE SoC
- TLSR9218, Telink 96MHz RISC-V BLE/Matter SoC
- UIS8910DM, RDA Cortex-A5 CAT-1 SoC
- V6211, Vango Cortex-M0/M4 G3/BPSK PLC SoC SoC
- V8850, RDA Cortex-A7 Cat.1 SoC
- V9400, Vango 26MHz Cortex-M0 SoC (IEC-62053)
- W801, winnermicro C-Sky V2 WiFi/BLE SoC
- W806, winnermicro C-Sky V2 MCU, W806 w/o WiFi/BLE
- W601, winnermicro 80MHz Cortex-M3 Wi-Fi SoC
- X2000, ingenic 1.2GHz MIPS CPU
- XL2409, XinLingGo 48MHz Cortex-M0 SoC
- XR809, Xradio 160MHz WiFi SoC
- XR872, Xradio 384MHz Cortex-M4F WiFi SoC
- XY1100E, xinyisemi NB-IoT SoC
- XY4100, xinyisemi Cat.1 SoC
- ZML165, ZLG 24bit ADC 48MHz Cortex-M0 SoC
- ZML166, ZLG 24bit ADC 48MHz Cortex-M0 SoC
- ZLG217, ZLG 96MHz Cortex-M0 MCU

### Micro/MPU/APSoC

- Loongson MCU and MPU (1C300B/2K1000), similiar but upgraded from MIPS
- HiSilicon (Huawei) ARMv8 Kirin/KunPeng 920
- Phytium FT-2000/4, SPARC and ARMv8
- HYGON, x86 CPU
- Zhaoxin, x86/ARM CPU
- Shenwei, Alpha CPU
- T-head, C-Cky V2 / RISC-V-64bit


- Unisoc, low-end 5G SoC
- A64, Quad-core ARM Cortex-A53, Allwinner, ARM/RISC-V SoC
- Amlogic, ARM SoC
- PX30, MPU SoC, Quad-core ARM Cortex-A35 quad core, Rockchip, ARM/RISC-V SoC
- RK3399PRO, MPU/NPU, SoC with NPU, Dual core A72 up to 1.8GHz, Quad-core A53 up to 1.4GHz, 3.0 TOPS NPU , Rockchip
- RV1126, MPU/MCU/NPU, Quad-core A7, RISC-V MCU, 2.0 TOPS NPU, Rockchip
- X2000, X1000, Ingenic, Xburst SoC


### Micro/DSP

- ADP32/AVP32/ADP16 32b-fix/32b-float/16b-fix DSP from Hunan Jinxin 
- MCU/MCU+DSP from Hongyun
- HXS32F28027 120MHz, 128KB flash, 12b ADC, FOC, RISC-V DSP from Haoxin
- HXS32F28034 120MHz, 256KB flash, LIN, 12b ADC, FOC, RISC-V DSP from Haoxin
- HXS32F28035 120MHz, 256KB flash, FPU/CLA/LIN, 12b ADC, FOC, RISC-V DSP from Haoxin
- HXS32F28335 200MHz, 1MB flash, FPU/2xCAN, RISC-V DSP from Haoxin
- HXS32F280020 48MHz, 64KB flash, 12b ADC, SSOP, RISC-V DSC from Haoxin
- HXS32F280025C 160MHz, 256KB flash, FPU/TMU/CLB/CAN, RISC-V DSP from Haoxin
- HXS32F280049C 160MHz, floating-point RISC-V H28x DSC from Haoxin
- HXS32F280103P 120MHz, 128KB flash, 2xADC, RISC-V MCU from Haoxin

### Micro/GPU

- Jinjia Micro, GPU
- lluvator CoreX, GPGPU for AI
- InnoSilicon, GPU

### Micro/NPU

- K210 RISC-V 400MHz Dual Core AIoT 0.2TOPS SoC
- K510 RISC-V 800MHz Dual Core AIoT 2.5TOPS SoC (pending)
- RK3399PRO, MPU/NPU, SoC with NPU, Dual core A72 up to 1.8GHz, Quad-core A53 up to 1.4GHz, 3.0 TOPS NPU , Rockchip
- RV1126, MPU/MCU/NPU, Quad-core A7, RISC-V MCU, 2.0 TOPS NPU, Rockchip
- RV1103/1106, Multicore RISC-V with NPU inside, Luckfox SBC
- CBV1800, Milticore RISC-V with NPU inside, Milk-V Duo SBC

--------------------------------------

### Sensors

- https://zhuanlan.zhihu.com/p/487265428
- https://zhuanlan.zhihu.com/p/408179798

- MEMS Sensors from QST
- SPW2430 MEMS microphone
- LD2420 24GHz Radar MEMS module
- Pressure XGZP6874 MEMS sensor 
- CDS871X, signal conditioning by chipsea, with AFE, ADC and 32bit MCU
- MMC5633NJL，3d magnet sensor from MEMSIC
- MXC35x0AL, 3d accelator from MEMSIC
- SC910GS, CMOS image sensor
- SH3001, 6DoF IMU
- GZ6826A, pressure sensor
- MXT2708A GNSS Baseband RF SoC
- VCSEL ToF light source
- AHT21 Temp/Hum sensor from Ao Song
- AGS10 gas sensor from Ao Song
- SC840 100A current sensor
- MDC04, Capacitor sensor
- M117/MTS01, high precision temperature sensor
- GXHTC3, high precision temperature sensor
- LiDAR from LuminWave
