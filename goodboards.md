## A Good Board Matters

Any silicon deserves a good evaluation board for marketing/development purposes.

- cost driven (reduce development and production cost) for the boards.
- easy to expand, either integrate or be integrated into other OSHW ecosystems.
- easy to hack, easy to solder manually. with solder jumpers, test points, heat isolator rings.
- tolerance for errors, add ESD, power protection circuits.
- easy to debug, with ob debugger or additional debugger facilities.

A lot of "professional" engineer under estimate the difficulities of evaluation boards. A good board should consider more requirements than the regular custom electronics designs.

## Color Board

There are many PCB with different color inks, green (default), red, black, yellow, blue and etc. And Jialichuang invented a color board, which can print bitmap onto the board. So a well-designed PCBA could be a art masterpiece iterally.