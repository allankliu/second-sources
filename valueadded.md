## Value Added Services

### For MCU

I am making a survey how to enhance these micros with value added services. So far there are some candidates:

- **Porting to GCC/make/cmake**, support GCC on x86/ARM, (consider to user AI to speed up)
- **Porting to Arduino**, with additional support for PIO/VSC, (consider to user AI to speed up);
- **Web serial**, Web based serial debugger with TypeScript, private deployment;
- **Web debug**, Web based network debugger with TypeScript, private deployment;
- **Web terminal**, Web based serial console terminal with TypeScript, custom made and private deployment;
- **Host programmer**, host serial programmer running on x86/ARM/RISC-V hosts, (consider to user AI to speed up)
- **SWD/JTAG programmer**, host serial programmer running on x86/ARM/RISC-V hosts, (consider to user AI to speed up)
- **SWD/JTAG programmer**, JLINK/DAPlink/sysfs openocd running on x86/ARM/RISC-V hosts.
- **elf loader**, More suitable for SPI flash based baremetal/RTOS.
- **mbed crypto**, mbed SSH porting and works with main stream security suites such as OpenSSL/OpenSSH.
- **SM crypto**, cryptographic for SM2/SM4.
- **uBASIC**, simple scripting such as legacy BASIC, which is still useful.
- **uLua**, simple scripting for embedded Lua.
- **uPython**, modern scripting such as MicroPython or eLua;
- **uJavaScript**, JavaScript runtime for embedded system.
- **DeviceScript**, runtime from MicroSoft.
- **uRust**, simple Rust runtime.

Also, there are more middleware, which are inspirated from some open source projects, we are going to iterate them.

- **Sub-1GHz**, libmesh, libtree, libleaf, radiolib, supporting local RFIC with enhanced link budget with MIMO antennna.
- **Rule-engine**, libpeer, libgroup, groupware with custom application logic, so changing model parameters can change behaviour.
- **license**, an embedded license management library.
- **FOTA**, libota, custom firmware Over The Air upgrade service.
- **spiffs**, embedded file system optimized for embedded flash and SPI flash.
- **dt-types**, a binary data type reprensentations.
- **BJSON**, a binary JSON serial/deserial.
- **dev-params**, a device params storage and access library.
- **dev-signature**, a device signature.
- **logging**, an embedded logging facility.
- **dev-annoucement**, a device annoucement in a adhoc network.
- **crypto**, an embedded cryptographic library with SM standards.
- **DNN**, an embedded DNN library
- **CNN**, an embedded CNN library
- **FFT**, an embedded DSP library
- **emloader**, an embedded library to access SWD/JTAG/SPI to the target devices.
- **Motor**, PWM output libraries for motor driver.
- **GUI**, GUI library
- **NFC**, NFC library
- **USB**, USB deivce (CDC/ACM/MSD/HID), USB host (CDC/ACM/MSD/HID)
- **uIP**, porting to devices 
- **Modbus**, porting to devices
- **CAN**, porting to devices
- **GPS**, porting to devices
- **VT100**, VT100 terminal

### For MPU

- **buildroot**, an embedded Linux for local ARM/RISC-V cores.
- **DNN**, an embedded DNN library
- **CNN**, an embedded CNN library
- **openOCD**, a debugger interface with sysio, and commercial debugger, will be available with webserial.
- **openWRT**, a wireless router distribution.
- **openWingMan**, a slave controller working with MPU host.

### For FPGA

- **CoreIP**, core IP of FPGA
- **PeripheralIP**, IP for peripherals 
- **AdvancedIP**, Custom made IP block for FPGA
- **OpenRISCV**, an embedded RISC-V soft-core
- **SDR**, an embedded software defined radio
- **AIoverFPGA**, FPGA based AI 
- **AIoverFPSoC**, FPSoC based AI

### Maker Oriented 

- **PCBA**, custom made for specific requirements
- **Supply chain management**, BOM and supply chain management