## What are Second Sources?


Although the semiconductors market is a mature enough, there are more and more fabless start-ups in China. The product line ranges from FPGA and memory to MCU/SoC and analog ICs. Recently, the authority has an aggressive plan to invest thousands of billions CNY in projects to advance semiconductors from design to production. It is easy to forecast that there will be more and more Chinese silicons available for the worldwide market.


In comparison to market titans like Intel, Ndivia, TI, NXP, STM, ADI, and Maxim, these small vendors may not be our first choice, but they can be **second sources."


## Original Design vs. Clones


Although many second source products were designed as clones of the original design, for example, a Cortex-M or RISC-V MCU with compatible register, peripheral, gpio allocation and more. However, many of the product lines are cost-driven. So there are many tricks inside the package.


### Embedded Flash vs. SPI Flash


To be frank, I was surprised to learn that some of the BLE SoCs are using SPI flash as code memory instead of embedded flash, which I am familiar with. Because SPI flash is much cheaper than the embedded flash IP purchase, they made such decisions. As a result, SPI usually connects to the CPU core with 1 or 2 wired data lines, and the SPI speed has some restriction due to the clock distribution network. So when people start to use them with the concept of embedded flash, they will find out that the GPIO toggling speed is extremely slow. So far, the MCU reports that the worst-case scenario is about mini second level, unless the code is loaded into RAM.So that is the reason why we can see a super big flash memory and a super big SRAM memory.


Some clever people add extra code cache between SPI flash and CPU code, but this introduces unpredictable latency during instruction cache misses. They will never tell you about it in advance.


After researching the Chinese second sources, I eventually understood why ARM considers NXP 128-bit embedded flash to be an advanced technology in the embedded system. By caching 128 bits into the cache, the read speed is 4 times faster than its competitors.


People may say that is nasty. With my extensive experience in semiconductors, the same things happened at those big giants, including every well-known brand. For example, a stupid proprietary 8bit MCU with unreasonable low price from a European supplier. That chip has a serious interrupt issue. However, they insist on selling it to the market.


## Applications are ineligible for Secondary Sources


- DO NOT USE IT with LIFE SUPPORT SYSTEMS.
- DO NOT USE IT with TIME-CRITICAL SYSTEMS.
- DO NOT USE IT with SECURITY SYSTEMS.


### Possible Programming Changes


If we look back in time, the memory configurations of the 8086/80286/80386 were similar: up to 66MHz 32-bit CPU, 1MB RAM, 1.44MB/2MB floppy, and 10/50MB HDD.


More technologies are now available: up to 240 MHz, even a sub-1 GHz 32 or 64-bit CPU; 256 KB of RAM, on-chip or off-chip (from 32 MB to 128 MB of PSRAM); 2 MB of SPI Flash; and multiple GB of microSD card.


We should change our design patterns according to these chips instead of sticking with embedded technologies.


## Second Sources with Facts


These chips are so attractive due to its price. A 240 MHz CPU with Bluetooth LE or WiFi costs less than one dollor. A BLE SoC quotes a price of less than 0.5 dollor. So we can make something with them anyway.


#### Tasks


I plan to evaluate these silicons using a standard benchmark, including but not limited to the following criteria:


- DMIPS benchmark (in SPI Flash and SRAM)
- standard peripheral drivers
- drivers for specialized peripherals (such as audio, USB, and video).


#### Value Added Services


In order to make it easier to design with these second sources, I plan to team up with partners to offer the following services:


- security algorithms
- Arduino/PlatformIO/VSC integration
- bootloader customization
- scripting language porting


Of course, the semiconductors are diverse, the above services are only dedicated for MCU/SoC.


## About the author(s)


Allan K Liu, a software engineer and technical writer, has worked for Philips/NXP semiconductors for 15 years. And then he has worked for some start-ups in the consumer robot and wireless mesh industries.
He wrote a book, "Python full stack development, from chip to cloud,"  published by PHEI Group in simplified and traditional Chinese.

